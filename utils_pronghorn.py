import requests
import os
from utils_DIA_TS import CUSTOMER_ERROR

P_USERNAME = os.environ['USERNAME_FOR_PRONGHORN']
P_PASSWORD = os.environ['PASSWORD_FOR_PRONGHORN']

# command templates & variables needed to run these templates for ASR920 & ASR9K - subject to expanding
C_TEMPLATES = {
    'DIA-TURNUP-MOP-ASR920-DOT1AD': ['CUST-VRF-NAME', 'BRIDGE-ID', 'CUSTOMER-ROUTER-IP'],
    'DIA-TURNUP-MOP-ASR920-DOUBLE-TAG-DOT1AD': ['CUST-VRF-NAME', 'BRIDGE-ID', 'CUSTOMER-ROUTER-IP'],
    'DIA-TURNUP-MOP-ASR920-SINGLETAG': ['CUST-VRF-NAME', 'BRIDGE-ID', 'CUSTOMER-ROUTER-IP'],
    'DIA-TURNUP-MOP-ASR920-DOUBLETAG': ['CUST-VRF-NAME', 'BRIDGE-ID', 'CUSTOMER-ROUTER-IP'],
    'DIA-TURNUP-MOP-ASR9K-DOT1AD': ['CUST-VRF-NAME', 'INTERFACE-TYPE', 'INTERFACE-ID', 'DOT1AD', 'CUSTOMER-ROUTER-IP'],
    'DIA-TURNUP-MOP-ASR9K-DOUBLE-TAG-DOT1AD': ['CUST-VRF-NAME', 'INTERFACE-TYPE', 'INTERFACE-ID', 'DOT1AD', 'DOT1Q', 'CUSTOMER-ROUTER-IP'],
    'DIA-TURNUP-MOP-ASR9K-SINGLETAG': ['CUST-VRF-NAME', 'INTERFACE-TYPE', 'INTERFACE-ID', 'DOT1Q', 'CUSTOMER-ROUTER-IP'],
    'DIA-TURNUP-MOP-ASR9K-DOUBLETAG': ['CUST-VRF-NAME', 'INTERFACE-TYPE', 'INTERFACE-ID', 'DOT1Q', 'SECOND-DOT1Q', 'CUSTOMER-ROUTER-IP']
}


def get_instance_from_pronghorn(circuit_ID):
    url = 'https://198.19.255.36:3443/service_management/getInstance/%2Fgranite-services:dia/' + circuit_ID
    resp = requests.get(url=url, auth=(P_USERNAME, P_PASSWORD), verify=False)
    data = resp.json()

    # when wrong circuit_ID was provided
    if '/granite-services:dia' not in data:
        raise CUSTOMER_ERROR(str(data[0]))
    else:
        return data


def run_c_template_from_pronghorn(data_for_c_template):
    url = 'https://198.19.255.36:3443/mop/RunCommandTemplate'
    resp = requests.post(url=url, auth=(P_USERNAME, P_PASSWORD),
                         json=data_for_c_template, verify=False)
    data = resp.json()
    return data


def get_values_of_variables(instance, *vrs):
    values = []
    for vari in vrs:
        if vari == 'CORE-IP':
            core_ip = instance['/granite-services:dia'][0]['IP-DETAILS'][0]['CORE-IP']
            values.append(core_ip)
        if vari == 'CUSTOMER-ROUTER-IP':
            customer_ip = instance['/granite-services:dia'][0]['IP-DETAILS'][0]['CUSTOMER-ROUTER-IP']
            values.append(customer_ip)
        if vari == 'DEVICE-TYPE':
            device_type = instance['/granite-services:dia'][0]['DEVICE-DETAILS'][0]['DEVICE-TYPE']
            values.append(device_type)
        if vari == 'ENCAPSULATION-TYPE':
            enca_type = instance['/granite-services:dia'][0]['SERVICE-DETAILS'][0]['ENCAPSULATION-TYPE']
            values.append(enca_type)
        if vari == 'CUST-VRF-NAME':
            cvn = instance['/granite-services:dia'][0]['SERVICE-DETAILS'][0]['CUST-VRF-NAME']
            values.append(cvn)
        if vari == 'BRIDGE-ID':
            bi = instance['/granite-services:dia'][0]['SERVICE-DETAILS'][0]['BRIDGE-ID']
            values.append(bi)
        if vari == 'INTERFACE-TYPE':
            it = instance['/granite-services:dia'][0]['DEVICE-DETAILS'][0]['INTERFACE-TYPE']
            values.append(it)
        if vari == 'INTERFACE-ID':
            ii = instance['/granite-services:dia'][0]['DEVICE-DETAILS'][0]['INTERFACE-ID']
            values.append(ii)
        if vari == 'DOT1AD':
            dot1ad = instance['/granite-services:dia'][0]['SERVICE-DETAILS'][0]['DOT1AQ']
            values.append(dot1ad)
        if vari == 'DOT1Q':
            dot1q = instance['/granite-services:dia'][0]['SERVICE-DETAILS'][0]['DOT1Q']
            values.append(dot1q)
        if vari == 'SECOND-DOT1Q':
            sdot = instance['/granite-services:dia'][0]['SERVICE-DETAILS'][0]['SECOND-DOT1Q']
            values.append(sdot)
        if vari == 'DEVICE-NAME':
            dn = instance['/granite-services:dia'][0]['DEVICE-DETAILS'][0]['DEVICE-NAME']
            values.append(dn)
    return values


def get_used_c_template(instance):
    device_type, enca_type = get_values_of_variables(
        instance, 'DEVICE-TYPE', 'ENCAPSULATION-TYPE')
    if enca_type == 'SINGLE-TAG':
        enca_type = 'SINGLETAG'
    if enca_type == 'DOUBLE-TAG':
        enca_type = 'DOUBLETAG'
    temp_str = device_type + '-' + enca_type
    print(temp_str)
    for temp in C_TEMPLATES:
        if temp.endswith(temp_str):
            return temp
    raise CUSTOMER_ERROR(
        f'New command template was added: <{device_type}>, <{enca_type}>')

