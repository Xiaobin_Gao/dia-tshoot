'''
    Script uses netdev module to talk to devices, but currently doesn't make use of its async mechanism.
'''

from flask import Flask, request, render_template, flash, redirect, url_for, session, jsonify
from utils_DIA_TS import extract_version_info_from_out, extract_interfaces_info_from_out, \
    extract_fiber_info_from_out, extract_ping_info_from_out, get_public_interface_ips, \
    NOT_ASR920_OR_ISR400_DEVICE_ERROR, CUSTOMER_ERROR
from utils_pronghorn import get_instance_from_pronghorn, get_used_c_template, \
    get_values_of_variables, run_c_template_from_pronghorn, \
    C_TEMPLATES
import os
import netdev
import asyncio
from datetime import datetime


app = Flask(__name__)
app.secret_key = b'\xf0\xf6\xefz\xbf9\x8d\xe8\xa1v\xfah\xb6~\xe1\x90'

USERNAME = os.environ['AAA_USERNAME']
PASSWORD = os.environ['AAA_PASSWORD']

EXCEPTIONS = (netdev.exceptions.DisconnectError, netdev.exceptions.TimeoutError,
              NOT_ASR920_OR_ISR400_DEVICE_ERROR, CUSTOMER_ERROR)

# errors to be excluded from interface error history
E_ERRORS = ('input_transfer_rate', 'interface_resets', 'lost_carrier',
            'multicast', 'output_transfer_rate', 'unknown_protocol_drops')


# get device model & uptime
async def get_dmodel_uptime(devi_param):
    async with netdev.create(**devi_param) as devi:
        out = await devi.send_command("show version")
    version_info = extract_version_info_from_out(out)
    device_model = version_info['model']
    device_uptime = version_info['uptime']
    if device_model.startswith('ASR-920'):
        return 'ASR920', device_uptime
    elif device_model.startswith('ISR4'):
        return 'ISR4000', device_uptime
    else:
        raise NOT_ASR920_OR_ISR400_DEVICE_ERROR(devi_param['host'])


# get error history of interfaces
async def get_inters_error_history():
    async with netdev.create(**session['cpe_param']) as cpe:
        is_e_h = {}
        pre_t_r = session['time_record']
        now_t_r = datetime.now()
        if (now_t_r - pre_t_r).seconds < 60:
            await asyncio.sleep(60 - (now_t_r - pre_t_r).seconds)
        out = await cpe.send_command("show interfaces")
    intes_info = extract_interfaces_info_from_out(out)
    pre_is_e_h = session['interfaces_error_history']
    for inte in pre_is_e_h:
        is_e_h[inte] = {key: float(intes_info[inte]['error_history'][key]) - float(
            pre_is_e_h[inte][key]) for key in pre_is_e_h[inte]}
    return is_e_h


# provision broad data for DIA troubleshooting when CPE is up
async def provision_DIA_tshoot(cpe_param, cpe_model, core_ip_from_p, customer_ip_from_p):

    # [[interface, status, protocol-status, duplex], [], [], ...]
    interfaces_status = []
    interfaces_light_level = []  # [[interface, temperature, tx, rx], [], [], ...]
    # [[public-ip/customer-ip, 8.8.8.8/1.1.1.1/core-ip, size, repeat, success-rate, avg], [], [], ...]
    interfaces_ping = []
    excepts = []   # ['*', '', '', ...]

    interfaces_error_history = {}     # {interface: {}, '': {}, '': {}, ...}
    time_record = None

    async with netdev.create(**cpe_param) as cpe:

        # ping from customer IP to core IP -> capture provided circuit ID and provided IP don't match
        for size in '', '1500':
            if size == '':
                command = f"ping {core_ip_from_p} source {customer_ip_from_p} repeat 100"
            else:
                command = f"ping {core_ip_from_p} source {customer_ip_from_p} repeat 100 size {size}"
            out = await cpe.send_command(command)
            if 'Invalid source address' in out:  # raise exception when provided IP address abd circuit ID NOT matched
                raise CUSTOMER_ERROR(f'IP Address, circuit ID NOT matched')
            ping_info = extract_ping_info_from_out(out)
            inte_ping = [customer_ip_from_p, core_ip_from_p, size,
                         '100', ping_info['success_rate'], ping_info['avg']]
            interfaces_ping.append(inte_ping)

        # provision interfaces status & interfaces error history for ISR4000 and ASR920 respectively
        si_out = await cpe.send_command("show interfaces")
        time_record = datetime.now()
        interfaces_info = extract_interfaces_info_from_out(si_out)
        if cpe_model == 'ISR4000':
            for interface in 'GigabitEthernet0/0/0', 'GigabitEthernet0/0/1':
                if interface not in interfaces_info:
                    excepts.append(f'* Missed interface: {interface}')
                    continue
                inte_status = [interface, interfaces_info[interface]
                               ['status'], interfaces_info[interface]['protocol_status'], interfaces_info[interface]['duplex']]
                interfaces_status.append(inte_status)

                # capture interface error history for ISR4000
                interfaces_error_history[interface] = {k: interfaces_info[interface]['error_history'][k]
                                                       for k in interfaces_info[interface]['error_history'] if k not in E_ERRORS}

                # check interface light levels for ISR4000 only when its media type is NOT RJ45
                if interfaces_info[interface]['media_type'] != 'RJ45':
                    if interface == 'GigabitEthernet0/0/0':
                        out = await cpe.send_command("show hw-module subslot 0/0 transceiver 0 status")
                    else:
                        out = await cpe.send_command("show hw-module subslot 0/0 transceiver 1 status")
                    fiber_info = extract_fiber_info_from_out(out, 'ISR4000')
                    inte_light_level = [
                        interface, fiber_info['temperature'], fiber_info['tx'], fiber_info['rx']]
                    interfaces_light_level.append(inte_light_level)

        if cpe_model == 'ASR920':
            for interface in 'GigabitEthernet0/0/0', 'GigabitEthernet0/0/1', 'TenGigabitEthernet0/0/2', 'TenGigabitEthernet0/0/3':
                if interface not in interfaces_info:
                    excepts.append(f'* Missed interface: {interface}')
                    continue
                inte_status = [interface, interfaces_info[interface]
                               ['status'], interfaces_info[interface]['protocol_status'], interfaces_info[interface]['duplex']]
                interfaces_status.append(inte_status)

                # capture interface error history for ASR920
                interfaces_error_history[interface] = {k: interfaces_info[interface]['error_history'][k]
                                                       for k in interfaces_info[interface]['error_history'] if k not in E_ERRORS}

            # check light levels of interfaces for ASR920
            out = await cpe.send_command("show interfaces transceiver")
            fiber_info = extract_fiber_info_from_out(out, 'ASR920')
            for interface in fiber_info:
                inte_light_level = [interface, fiber_info[interface]['temperature'],
                                    fiber_info[interface]['tx'], fiber_info[interface]['rx']]
                interfaces_light_level.append(inte_light_level)

        # ping from public interfaces to the internet
        # filter out public interfaces that fail to ping to avoid long waiting
        public_interface_ips = get_public_interface_ips(si_out)
        ping_from_ips = list(public_interface_ips.values())
        ping_to_ips = ['1.1.1.1', '8.8.8.8']
        rm_from_ips = []
        for interface in public_interface_ips:
            f_count = 0
            for to_ip in ping_to_ips:
                command = f"ping {to_ip} source {public_interface_ips[interface]}"
                out = await cpe.send_command(command)
                ping_info = extract_ping_info_from_out(out)
                if ping_info['success_rate'] == '0':
                    f_count += 1
            if f_count == len(ping_to_ips):
                rm_from_ips.append(public_interface_ips[interface])
                excepts.append(
                    f'* Ping check failed: {public_interface_ips[interface]} - {interface}')
        ping_from_ips = [x for x in ping_from_ips if x not in rm_from_ips]

        for from_ip in ping_from_ips:
            for to_ip in ping_to_ips:
                for size in '', '1500':
                    if size == '':
                        command = f"ping {to_ip} source {from_ip} repeat 100"
                    else:
                        command = f"ping {to_ip} source {from_ip} repeat 100 size {size}"
                    out = await cpe.send_command(command)
                    ping_info = extract_ping_info_from_out(out)
                    inte_ping = [from_ip, to_ip, size, '100',
                                 ping_info['success_rate'], ping_info['avg']]
                    interfaces_ping.append(inte_ping)

    # store error history of interfaces & time record in user session for future query
    session['interfaces_error_history'] = interfaces_error_history
    session['time_record'] = time_record

    print('------------>')
    print({
        'interfaces_status': interfaces_status,
        'interfaces_light_level': interfaces_light_level,
        'interfaces_ping': interfaces_ping,
        'excepts': excepts
    })
    print('<------------')

    return interfaces_status, interfaces_light_level, interfaces_ping, excepts


def troubleshoot_when_CPE_down(instance_p):
    warning = []
    success = []

    # check which command template was used - "DEVICE-TYPE", "ENCAPSULATION-TYPE"
    c_template = get_used_c_template(instance_p)

    # get variables and their values needed to run the command template
    vars_for_temp = C_TEMPLATES[c_template]
    values_for_t_vars = get_values_of_variables(instance_p, *vars_for_temp)
    variables = dict(zip(vars_for_temp, values_for_t_vars))

    # run the command template by making /mop/RunCommandTemplate call to pronghorn
    devices = get_values_of_variables(instance_p, 'DEVICE-NAME')

    result = run_c_template_from_pronghorn(dict(zip(['template', 'devices', 'variables'],
                                                    [c_template, devices, variables])))
    
    print('<-------------------------------------')
    print(f'* Command Template Used:')
    print(c_template)
    print(f'* Variables for Template:')
    print(variables)
    print(f'* Devices:')
    print(devices)
    import pprint
    print('* Running Results of Command:')
    pprint.pprint(result)
    print('-------------------------------------->')

    print('<------------------')
    for item in result['commands_results']:
        if item['result'] == False:
            warning.append(item['command'])

            print('* Fail Command & Response:')
            print(item['command'])
            print(item['response'])
        else:
            success.append(item['command'])

            print('* Success Command & Response:')
            print(item['command'])
            print(item['response'])
    print('------------------->')
    return warning, success


@app.route('/')
def index():
    down = request.args.get('down')
    if down == '1':
        return render_template('index.html', down=down)
    return render_template('index.html')


@app.route('/DIATroubleshooting', methods=['POST'])
def DIA_troubleshooting():
    host = request.form['ip']
    circuit_ID = request.form['cid']
    if not host or not circuit_ID:
        flash('Please enter all fields', 'required')
        return redirect('/')
    else:
        cpe = {
            'username': USERNAME,
            'password': PASSWORD,
            'device_type': 'cisco_ios',
            'host': host
        }

        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        try:
            cpe_model, cpe_uptime = loop.run_until_complete(
                get_dmodel_uptime(cpe))
            inst = get_instance_from_pronghorn(circuit_ID)
            core_ip, customer_ip = get_values_of_variables(
                inst, 'CORE-IP', 'CUSTOMER-ROUTER-IP')
        except EXCEPTIONS as e:
            if isinstance(e, CUSTOMER_ERROR):  # catch exception caused by: invalid circuit ID
                flash(str(e) + ': ' + circuit_ID, 'error')
            else:                              # catch exceptions caused by: wrong login credentials, invalid IP address
                flash(str(e), 'error')
            return redirect('/')
        
        result = None
        while result == None:
            try:  # catch exception caused by: circuit ID and IP address NOT matched, and Timeout which is rare at this point
                result = loop.run_until_complete(asyncio.gather(
                    provision_DIA_tshoot(cpe, cpe_model, core_ip, customer_ip)))[0]
            except EXCEPTIONS as e:
                if isinstance(e, CUSTOMER_ERROR):
                    flash(str(e) + f': <{host}>, <{circuit_ID}>', 'error')
                    return render_template('index.html')
                else:
                    result = None

        info = [host, cpe_model, cpe_uptime]
        warning = [[], [], []]   # [[status], [light-levels], [ping]]
        success = [[], [], []]   # [[status], [light-levels], [ping]]
        excepts = result[3]

        # TDDO: Troubleshoot DIA when CPE is up
        # check interfaces status
        for inte_status in result[0]:
            if cpe_model == 'ASR920' and inte_status[0] in ('GigabitEthernet0/0/1', 'TenGigabitEthernet0/0/3'):
                if inte_status[2] == 'up':
                    success[0].append(f'{inte_status[0]} status checked')
                else:
                    warning[0].append(
                        f'{inte_status[0]} status: {inte_status[2]}')

            if cpe_model == 'ISR4000':
                if inte_status[2] == 'up':
                    success[0].append(f'{inte_status[0]} status checked')
                else:
                    warning[0].append(
                        f'{inte_status[0]} status: {inte_status[2]}')

            if inte_status[3] == 'Half Duplex':
                warning[0].append(
                    f'{inte_status[0]} duplex status: {inte_status[3]}')
            else:
                success[0].append(f'{inte_status[0]} duplex status checked')

        # check light levels
        for fiber in result[1]:
            tx = float(fiber[2])
            rx = float(fiber[3])
            if tx < -16 or tx > -1.5 or rx < -16 or rx > -1.5:
                warning[1].append(f'Fiber: {fiber[0]} - Tx: {tx}, Rx: {rx}')
            else:
                success[1].append(f'Fiber: {fiber[0]} light levels checked')

        # check ping
        for inte_ping in result[2]:
            if int(inte_ping[4]) <= 97:
                warning[2].append(
                    f'{inte_ping[0]} to {inte_ping[1]} success rate: {inte_ping[4]}')
            else:
                success[2].append(
                    f'{inte_ping[0]} to {inte_ping[1]} success rate: {inte_ping[4]}')

            if inte_ping[1] in ('1.1.1.1', '8.8.8.8'):
                if float(inte_ping[5]) > 120:
                    warning[2].append(
                        f'{inte_ping[0]} to {inte_ping[1]} average time: {float(inte_ping[5])}')
                else:
                    success[2].append(
                        f'{inte_ping[0]} to {inte_ping[1]} average time: {float(inte_ping[5])}')
            else:
                if float(inte_ping[5]) > 50:
                    warning[2].append(
                        f'{inte_ping[0]} to {inte_ping[1]} average time: {float(inte_ping[5])}')
                else:
                    success[2].append(
                        f'{inte_ping[0]} to {inte_ping[1]} average time: {float(inte_ping[5])}')

        data = [info, warning, success, excepts]

        session['cpe_param'] = cpe
        session['data'] = data

        return render_template('up-CPE-report.html', result=data)


@app.route('/DIATroubleshooting/showErrorHistory')
def show_i_e_histrory():
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    intes_error_history = None
    
    # catch exception caused by: Timeout, which is rare at this point
    while intes_error_history == None:
        try:
            intes_error_history = loop.run_until_complete(get_inters_error_history())
        except:
            intes_error_history = None
    intes_error = {}
    for inte in intes_error_history:
        intes_error[inte] = {k: intes_error_history[inte][k] for k in intes_error_history[inte]
                             if float(intes_error_history[inte][k]) > 0}
    wng = []
    sus = []
    for inte in intes_error:
        if intes_error[inte]:
            for i in intes_error[inte]:
                wng.append(f'{i} on {inte}: {intes_error[inte][i]}')
        else:
            sus.append(f'{inte} error history checked')

    data = session['data']
    data2 = [wng, sus]
    return render_template('up-CPE-report.html', result=data, result2=data2)


@app.route('/DIATroubleshooting/CPE-down', methods=['POST'])
def cpe_down():
    circuit_ID = request.form.get('cid')
    if not circuit_ID:
        flash('Please enter circuit ID', category='required')
        return redirect(url_for('index', down=1))
    try:  # catch exception caused by: invalid circuit ID
        inst = get_instance_from_pronghorn(circuit_ID)
    except CUSTOMER_ERROR as e:
        flash(str(e) + ': ' + circuit_ID, 'error')
        return redirect(url_for('index', down=1))

    try:  # catch exception caused by: command template newly added to Pronghorn
        warning, success = troubleshoot_when_CPE_down(inst)
    except CUSTOMER_ERROR as e:
        flash(str(e), 'error')
        return render_template('down-CPE-report.html')

    data = [circuit_ID, warning, success]
    return render_template('down-CPE-report.html', result=data)


if __name__ == '__main__':
    app.run(debug=True)
