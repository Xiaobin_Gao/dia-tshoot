# DIA(Dedicated Internet Access) Troubleshooting

> DIA Troubleshooting diagnoses a CPE(Customer-Premises Equipment) that is not working properly. When a CPE is accessible, DIA Troubleshooting checks interface status, fiber light levels, ping, and interface error records of its WAN interface and its LAN interface. An out-of-order CPE may be inaccessible, in the case of which it calls Pronghorn(internal Cisco software) for diagnoses.

## Run in Local Development

**> *Install Python3.6+ & virtualenv***

**> *Install dependencies***
  
    pip install -r requirements.txt

**> *Set environment variables(e.g. cmd)***
  
    set AAA_UN=x         # Username of a device management account   
    set AAA_PW=x         # Password
    set AD_UN=x          # Username of a Pronghorn account
    set AD_PW=x          # Password
    
**> *Run the app***
    
    python DIA_troubleshooting.py


## API Calls

#### Troubleshoot from the CPE end when it is accessible &#x25BE;

    POST: /DIATroubleshooting

###### REQUEST PARAMETERS
*Content-Type: application/json*
    
    ip     IP address of a CPE

###### SAMPLE RESPONSES

    # When the ip was not provided
    {"Error": "Missed CPE IP address"}

    # When the CPE is inaccessible/the Granite MPLS VPN is off
    {"Error": "Host 198.18.5.60 Timeout Error"}

    # The successful output 
    # The intent is to diagnose interface status, fiber light levels, and ping of the WAN interface and the LAN interface. The output diagnoses are stored in "success" and "warning"
    # Any of the intended checking targets might fail due to the out-of-order CPE, and therefore exceptions will be caught in "except"
    # "success" and "warning" follow the list pattern: [interface status, fiber light levels, ping]
    # "info" follows the list pattern: [host, model, uptime]
    # "seh_req_data" stores error records of all the detected interfaces. Its first list item is a time record of when this info is obtained
    {"excepts": ["No public LAN IP found: due to down LAN interface"],
     "info": ["198.18.11.208", "ASR920", "1 day, 3 minutes"],
     "seh_req_data": [
        1592923559.694492,
        {"GigabitEthernet0/0/0": {"babbles": "0",
                                  "collisions": "0",
                                  "crc": "0",
                                  "deferred": "0",
                                  "frame": "0",
                                  "giants": "0",
                                  "ignored": "0",
                                  "input_errors": "2",
                                  "late_collision": "0",
                                  "no_carrier": "0",
                                  "output_buffer_failures": "0",
                                  "output_buffers_swapped_out": "0",
                                  "output_errors": "0",
                                  "overrun": "0",
                                  "pause_input": "0",
                                  "pause_output": "0",
                                  "runts": "0",
                                  "throttles": "0",
                                  "underruns": "0",
                                  "watchdog": "0"
                                 },...
        "TenGigabitEthernet0/0/3": {"babbles": "0",...,
                                    "watchdog": "0"
                                 }
        }],
     "success": [["WAN GigabitEthernet0/0/0 duplex checked", "LAN GigabitEthernet0/0/1 duplex checked"], [],
                 ["169.254.1.210 to 169.254.1.209 size 100 success rate: 100", "169.254.1.210 to 169.254.1.209 size 100 average time: 2.0",
                 "169.254.1.210 to 169.254.1.209 size 1500 success rate: 98", "169.254.1.210 to 169.254.1.209 size 1500 average time: 2.0"]],
     "warning": [["LAN interface: down"], ["Fiber Te0/0/2: Tx -5.1, Rx -30.0"], []]}

#### Compare current interface error records with those provided &#x25BE;
    POST: /DIATroubleshooting/showErrorHistory

###### REQUEST PARAMETERS
*Content-Type: application/json*

    cpe_ip            IP address of a CPE
    dmodel            Device model of the CPE('ASR920' or 'ISR4000')
    is_e_h            Error records of interfaces, which can be obtained by calling 'POST: /DIATroubleshooting'
    time_record       Time record of when is_e_h is obtained

###### SAMPLE RESPONSES
    # When the cpe_ip was not provided  
    {"Error": "Missed CPE IP address"}

    # When the dmodel was not provided
    {"Error": "Missed Device Model"}

    # The successful output
    {"success": ["WAN interface error history checked", "LAN interface error history checked"], "warning": []}


#### Troubleshoot from the Core end when a CPE is inaccessible &#x25BE;
    POST: /DIATroubleshooting/CPE-down

###### REQUEST PARAMETERS
*Content-Type: application/json*

    cid        Circuit ID

###### SAMPLE RESPONSES

    # The successful output
    # List pattern: [circuit ID, failed commands from the Core end, successful commands from the Core end]
    {'NSODIA0000000396', ['show ip arp vrf INTERNET_CUSTOMER-QUI920_POP BDI 3008 | i 169.254.1.210', 
                         'ping vrf INTERNET_CUSTOMER-QUI920_POP 169.254.1.210', 
                         'sh bgp vpnv4 unicast vrf INTERNET_CUSTOMER-QUI920_POP summary | include 169.254.1.210', 
                         'show interfaces BDI 3008 accounting'], []}